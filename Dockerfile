FROM docker.io/library/debian:stable-slim AS build
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends git ca-certificates
RUN git clone --depth=1 https://git.schokokeks.org/git/freewvs.git /src

FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:master
RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends python3 python-is-python3 python3-pip python3-requests python3-backoff && \
    rm -fr /var/lib/apt/lists/*
COPY --from=build /src/freewvs /usr/bin/freewvs
COPY --from=build /src/freewvsdb/ /var/lib/freewvs
COPY --from=build /src/update-freewvsdb /usr/bin/update-freewvsdb
COPY --chmod=0755 upload.py /upload.py
COPY --chmod=0755 scan.sh /scan.sh
COPY conf/ /etc/

