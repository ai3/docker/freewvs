Container image to run *freewvs* periodically and upload the results
to the aux-db.

Since we have to resolve resource IDs (website names) from the
filesystem paths, we need access to the homedirs.json file.
