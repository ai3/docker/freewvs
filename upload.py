#!/usr/bin/python3

from xml.etree import ElementTree
import argparse
import json
import logging
import os
import sys
import time
import requests


DATA_TYPE = 'cms_info'
DATA_TTL = 3 * 86400


class HomedirMap():

    def __init__(self, data):
        self._by_docroot = dict(
            (x['docroot'], x) for x in data)

    def lookup(self, path):
        """Look up a path (or its parents) in the map."""
        while path not in ('', '/'):
            if path in self._by_docroot:
                return self._by_docroot[path]
            path = os.path.split(path)[0]
        return None


class StatusError(Exception):

    def __init__(self, resp):
        super(StatusError, self).__init__('%s %s: HTTP error %d: %s' % (
            resp.request.method, resp.request.url, resp.status_code, resp.text))


class Backend():

    def __init__(self, url, ssl_cert=None, ssl_key=None,
                 ssl_ca=None, timeout=60):
        self._url = url
        self._session = requests.Session()
        if ssl_cert and ssl_key:
            self._session.cert = (ssl_cert, ssl_key)
            self._session.verify = ssl_ca
        self._session.timeout = timeout

    def submit(self, data):
        req = self._session.prepare_request(
            requests.Request(
                'POST',
                self._url,
                json=data))
        resp = self._session.send(req)
        status = resp.status_code
        if status != 200:
            raise StatusError(resp)
        return resp


def parse_xml_app_entry(app):
    # Make a dictionary with all the tags.
    data = dict((x.tag, x.text) for x in app.iter())
    # State is an attribute on the app tag.
    data['state'] = app.attrib.get('state')
    return data


def read_input(fd, homedir_map):
    tree = ElementTree.parse(fd)
    entries = []
    for app in tree.iter('app'):
        entry = parse_xml_app_entry(app)

        # Take the 'homedir' attr out of the entry values and make it
        # the aux-db secondary key.
        homedir = entry.pop('directory')
        resource = homedir_map.lookup(homedir)
        if not resource:
            continue
        if resource['status'] not in ('active', 'readonly'):
            continue

        entries.append({
            'resource_id': resource['dn'],
            'app_key': homedir,
            'value_json': json.dumps(entry),
        })
    return entries


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--report-url', metavar='url')
    parser.add_argument('--ssl-cert', metavar='file')
    parser.add_argument('--ssl-key', metavar='file')
    parser.add_argument('--ssl-ca', metavar='file')
    parser.add_argument('--homedir-map', metavar='file',
                        default='/var/lib/ai/web-users-info.json')
    opts = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    with open(opts.homedir_map, 'rb') as fd:
        homedir_map = HomedirMap(json.load(fd))
    be = Backend(opts.report_url, opts.ssl_cert, opts.ssl_key, opts.ssl_ca)
    entries = read_input(sys.stdin, homedir_map)
    logging.info('submitting %d entries...', len(entries))
    timestamp = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())
    resp = be.submit({
        'type': DATA_TYPE,
        'ttl': DATA_TTL,
        'timestamp': timestamp,
        'entries': entries,
    })
    if resp.status_code != 200:
        logging.error('submission failed (HTTP status %d)', resp.status_code)
        sys.exit(1)
    logging.info('submission successful')


if __name__ == '__main__':
    main()
