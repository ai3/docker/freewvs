#!/bin/sh

set -u

nice ionice -c 3 freewvs --all --xml $* \
    | /upload.py ${UPLOAD_ARGS}

